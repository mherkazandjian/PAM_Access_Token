# Build Constants
PROJECT = pam_access_token
VERSION := $(shell cat .version)
CHIPSET ?= amd64
DESTINATION ?= /usr/lib/security
MAINTAINER ?= Harry Kodden
CONTACT ?= SURF
CMAKE_VERSION = 3.26
NLOHMANN_VERSION = 3.11.2
BEARER = secret
ATTRIBUTE = uid
USERNAME = testuser
LOGLEVEL = DEBUG
DOCKERFILE_BUILD ?= Dockerfile.build.centos
DOCKERFILE_TEST ?= Dockerfile.test.ubuntu

-include $(PWD)/.env

IDP ?= https://example.com

# Docker specification
BUILD_IMAGE = builder
BUILDER = docker run --rm \
	--workdir /project \
	--volume `pwd`/project:/project \
	--volume `pwd`/packages:/packages \
	${BUILD_IMAGE}

TEST_IMAGE = tester
TESTER = docker run --rm -ti \
	--volume `pwd`/${PROJECT}.pam:/etc/pam.d/test \
	--volume `pwd`/${PROJECT}.conf:/etc/${PROJECT}.conf \
	--volume `pwd`/access_token.sh:/usr/local/bin/access_token.sh \
	${TEST_IMAGE}

# Makefile recipies
all: packages

.build-image:
	docker build . -f ${DOCKERFILE_BUILD} -t ${BUILD_IMAGE} --platform linux/${CHIPSET} \
		--build-arg PROJECT="${PROJECT}" \
		--build-arg DESTINATION="${DESTINATION}" \
		--build-arg MAINTAINER="${MAINTAINER}" \
		--build-arg CONTACT="${CONTACT}" \
		--build-arg VERSION="${VERSION}" \
		--build-arg CHIPSET="${CHIPSET}"
	@touch .build-image

.test-image:
	docker build . -f ${DOCKERFILE_TEST} -t ${TEST_IMAGE} --platform linux/${CHIPSET} \
		--build-arg PROJECT="${PROJECT}" \
		--build-arg VERSION="${VERSION}" \
		--build-arg CHIPSET="${CHIPSET}"

cmake: .build-image
	${BUILDER} cmake -B /project/bin -S /project/src

cli: .build-image
	${BUILDER} bash

build: cmake
	${BUILDER} make -C bin

packages: build
	${BUILDER} make -C bin package
	@echo "auth required ${DESTINATION}/${PROJECT}.so /etc/${PROJECT}.conf" > ${PROJECT}.pam
	@echo "{\n\
		\"wellknown_endpoint\": \"${IDP}/.well-known/openid-configuration\", \n\
		\"user_attribute\": \"${ATTRIBUTE}\", \n\
		\"loglevel\": \"${LOGLEVEL}\", \n\
		\"entitlements\": \"*\" \n\
	}"> ${PROJECT}.conf

test: packages .test-image
	@echo "*** Packages produced:"
	@ls -l ./packages/*.*
	@echo
	@echo "*** file: ${PROJECT}.conf: (Adjust this to your needs)"
	@cat ${PROJECT}.conf
	@echo
	@echo "*** file: ${PROJECT}.pam:"
	@cat ${PROJECT}.pam
	@echo
	@echo "*** Do your tests via:"
	@echo "$$ ${TESTER} bash"
	@echo
	@echo "*** Example:"
	@echo "$$ useradd -m ${USERNAME}"
	@echo "$$ echo \"<your access token>\" > /home/testuser/.access_token"
	@echo "$$ pamtester test ${USERNAME} authenticate"

clean:
	-docker image rmi ${BUILD_IMAGE} ${TEST_IMAGE} 2>/dev/null
	@rm -rf .build-image project/bin packages *.conf *.pam
