#!/bin/bash

if [ -f .env ]; then
  source .env
fi

IDP_URL=${IDP:-"http://localhost"}
CLIENT_ID=${CLIENT_ID:-"test-client"}

echo "IDP: $IDP..."
echo "CLIENT: $CLIENT_ID"

SCOPE="openid uid"

configuration=$(curl --silent --request GET --url "${IDP_URL}/.well-known/openid-configuration")

device_authorization_endpoint=$(echo "$configuration" | jq -r '.device_authorization_endpoint')
token_endpoint=$(echo "$configuration" | jq -r '.token_endpoint')
token_introspection_endpoint=$(echo "$configuration" | jq -r '.token_introspection_endpoint')
userinfo_endpoint=$(echo "$configuration" | jq -r '.userinfo_endpoint')

response=$(curl --silent --request POST \
  --url "${device_authorization_endpoint}" \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data "client_id=${CLIENT_ID}" \
  --data "scope=${SCOPE}")

COLOR='\033[1;33m' # Yellow
BLANK='\033[0m' # No Color

verification_uri_complete=$(echo $response | jq -r '.verification_uri_complete')
device_code=$(echo $response | jq -r '.device_code')
expires_in=$(echo $response | jq -r '.expires_in')
interval=$(echo $response | jq -r '.interval')

echo "Click on this link to proceed..."
echo -e "${COLOR}$verification_uri_complete${BLANK}"

while [ $expires_in -gt 0 ]
do
  sleep $interval
  expires_in=$((expires_in-interval))

  response=$(curl --silent --request POST \
    --url "${token_endpoint}" \
    --header 'Content-Type: application/x-www-form-urlencoded' \
    --data client_id="${CLIENT_ID}" \
    --data device_code="${device_code}" \
    --data grant_type=urn:ietf:params:oauth:grant-type:device_code)
    
  if echo $response | jq -e 'has("access_token")' > /dev/null; then
    access_token="$(echo "${response}" | jq -r '.access_token')"
 
    echo $access_token > ${HOME}/.access_token
  
    break
  fi
  
done
