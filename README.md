[![pipeline status](https://gitlab.kodden.nl/harry/PAM_Access_token/badges/main/pipeline.svg)](https://gitlab.kodden.nl/harry/PAM_Access_token/-/commits/main)

# Introduction

The repository produces a PAM module package for both RPM and Debian based Linux implementations. It offers a PAM Module to consume a locally stored OAuth Access Token and make a request to he OAuth Authorization Server to do UserInfo Introspection. When this succeeds and the returned user_attribute matches the username, the PAM Module gives succesfull result, otherwise it fails.

# Clone

Clone the repository:

```bash
git clone https://gitlab.kodden.nl/harry/PAM_Access_Token.git
```

This repository makes use of **Docker**. so please make sure you have **Docker** installed and running on your system

# Build

```bash
make
make build
make packages
make test
```

# Test

By running **make test** you will see following instructions:

```bash
*** Packages produced:
-rw-r--r-- 1 root root 223459 Feb 22 09:01 ./packages/pam_access_token-1.0.0-1.x86_64.rpm
-rw-r--r-- 1 root root 220942 Feb 22 09:01 ./packages/pam_access_token_1.0.0_amd64.deb

*** file: pam_access_token.conf: (Adjust this to your needs)
{
        "wellknown_endpoint": "< your OIDC provider>/.well-known/openid-configuration", 
        "user_attribute": "uid" 
}

*** file: pam_access_token.pam:
auth required pam_access_token.so /etc/pam_access_token.conf

*** Do your tests via:
$ docker run -ti --rm --volume ./pam_access_token.pam:/etc/pam.d/test --volume ./pam_access_token.conf:/etc/pam_access_token.conf tester bash

*** Example:
$ useradd -m testuser
$ echo "<your access token>" > /home/testuser/.access_token
$ pamtester test testuser authenticate
```

# Versioning

The versions of the packages created are tagged with the version as specified in the local file: **.version**. The  version value will be manually adjusted when relevant changes to the module have been implemented.
