#pragma once

#include <string>
#include <vector>
#include <map>

std::string form_url_encode(const std::map<std::string, std::string> & args);

std::string request(
  const std::string & url,
  const std::string & method,
  const std::map<std::string, std::string> & headers,
  const std::string & data
);