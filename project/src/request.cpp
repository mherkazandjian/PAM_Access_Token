extern "C" {
    #include <curl/curl.h>
    #include <string.h>

    #include "logging.h"
}

#include "request.h"
#include <stdexcept>

extern "C" {
typedef struct {
    char* payload;
    size_t size;
} CURL_FETCH;

struct data {
    char trace_ascii; /* 1 or 0 */
};

static
void dump(
    const char *text,
    unsigned char *ptr, size_t size,
    char nohex)
{
    size_t i;
    size_t c;
    FILE *stream = fopen("/tmp/dumper","w+");

    unsigned int width = 0x10;

    if(nohex)
        /* without the hex output, we can fit more on screen */
        width = 0x40;

    fprintf(stream, "%s, %10.10lu bytes (0x%8.8lx)\n",
                    text, (unsigned long)size, (unsigned long)size);

    for(i = 0; i<size; i += width) {

        fprintf(stream, "%4.4lx: ", (unsigned long)i);

        if(!nohex) {
            /* hex not disabled, show it */
            for(c = 0; c < width; c++)
                if(i + c < size)
                    fprintf(stream, "%02x ", ptr[i + c]);
                else
                    fputs("   ", stream);
        }

        for(c = 0; (c < width) && (i + c < size); c++) {
            /* check for 0D0A; if found, skip past and start a new line of output */
            if(nohex && (i + c + 1 < size) && ptr[i + c] == 0x0D &&
                ptr[i + c + 1] == 0x0A) {
                i += (c + 2 - width);
                break;
            }
            fprintf(stream, "%c",
                            (ptr[i + c] >= 0x20) && (ptr[i + c]<0x80)?ptr[i + c]:'.');
            /* check again for 0D0A, to avoid an extra \n if it's at width */
            if(nohex && (i + c + 2 < size) && ptr[i + c + 1] == 0x0D &&
                ptr[i + c + 2] == 0x0A) {
                i += (c + 3 - width);
                break;
            }
        }
        fputc('\n', stream); /* newline */
    }
    fflush(stream);
    fclose(stream);
}

static
int my_trace(CURL *handle, curl_infotype type,
                         char *data, size_t size,
                         void *userp)
{
    struct data *config = (struct data *)userp;
    const char *text;
    (void)handle; /* prevent compiler warning */

    switch(type) {
    case CURLINFO_TEXT:
        logging(LOG_ERR, "== Info: %s", data);
        /* FALLTHROUGH */
    default: /* in case a new one is introduced to shock us */
        return 0;

    case CURLINFO_HEADER_OUT:
        text = "=> Send header";
        break;
    case CURLINFO_DATA_OUT:
        text = "=> Send data";
        break;
    case CURLINFO_SSL_DATA_OUT:
        text = "=> Send SSL data";
        break;
    case CURLINFO_HEADER_IN:
        text = "<= Recv header";
        break;
    case CURLINFO_DATA_IN:
        text = "<= Recv data";
        break;
    case CURLINFO_SSL_DATA_IN:
        text = "<= Recv SSL data";
        break;
    }

    dump(text, (unsigned char *)data, size, config->trace_ascii);

    return 0;
}

static size_t curl_callback(void* contents, size_t size, size_t nmemb, void* userp) {
    size_t realsize = size * nmemb;         /* calculate buffer size */
    CURL_FETCH *p = (CURL_FETCH *) userp;   /* cast pointer to fetch struct */

    /* expand buffer */
        char * old_payload = p->payload;
    p->payload = (char*)realloc(old_payload, p->size + realsize + 1);

    /* check buffer */
    if (p->payload == NULL) {
                
            /* free buffer */
            if(old_payload) {
                free(old_payload);
            }
            p->size = 0;
        /* return */
        return (size_t) -1;
    }

    /* copy contents to buffer */
    memcpy(p->payload+p->size, contents, realsize);

    /* set new buffer size */
    p->size += realsize;

    /* ensure null termination */
    p->payload[p->size] = 0;

    /* return size */
    return realsize;
}

} // extern "C"

static std::string _url_encode_string(CURL* curl, const std::string & str) {
    char * esc_str = curl_easy_escape(curl, str.c_str(), str.size());
    std::string ret(esc_str);
    curl_free(esc_str);
    return ret;
}

std::string form_url_encode(const std::map<std::string, std::string> & args) {
    CURL* curl = curl_easy_init();
    std::string ret = "";

    bool first = true;
    for (const auto & p : args) {
        if(first) {
            first = false;
        } else {
            ret+= '&';
        }
        ret += _url_encode_string(curl, p.first) + "=" + _url_encode_string(curl, p.second);
    }

    curl_easy_cleanup(curl);

    return ret;
}

std::string request(
    const std::string & url,
    const std::string & method,
    const std::map<std::string, std::string> & headers,
    const std::string & data
) {
    CURL* curl;
    CURL_FETCH fetcher;
    struct data config;

    config.trace_ascii = 1; /* enable ascii tracing */

    // Prepare Headers...
    struct curl_slist* request_headers = NULL;

    for(const auto & h : headers) {
        std::string tmp = h.first;
        tmp += ": ";
        tmp += h.second;
        request_headers = curl_slist_append(request_headers, tmp.c_str());
    }

    fetcher.payload = NULL;
    fetcher.size = 0;
        
    // Prepare API request...
    curl = curl_easy_init();

    logging(LOG_ERR, "Request: %s %s %s\n", method.c_str(), url.c_str(), data.c_str());

    logging(LOG_ERR, "next...\n");

    if (curl) {
        long response_code, perform_status, response_info_code;

        if (logging_priority == LOG_DEBUG) {
            curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, my_trace);
            curl_easy_setopt(curl, CURLOPT_DEBUGDATA, &config);
            curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
        }

        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, method.c_str());
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, request_headers);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curl_callback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*)&fetcher);
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "pam_module");
        //       curl_easy_setopt(curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_3);

        if(data.size()) {
            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data.c_str());
            curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, data.size());
        }
        // Perform the request
        perform_status = curl_easy_perform(curl);
        
        std::string result(fetcher.size ? fetcher.payload : "");
        if(fetcher.payload) {
            free(fetcher.payload);
        }

        if (perform_status != CURLE_OK) {
            curl_easy_cleanup(curl);
            throw std::runtime_error(
                (std::string("curl_easy_perform(curl) != CURL_OK : ") + std::to_string(perform_status) +
                std::string(" url ") + url + 
                std::string(" method ") + method + 
                std::string(" data ") + result).c_str()
            );
        }

        // Check response
        response_info_code = curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
        if (response_info_code != CURLE_OK) {
            curl_easy_cleanup(curl);
            throw std::runtime_error(
                (std::string("curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE) != CURL_OK") +
                std::string(" url ") + url + 
                std::string(" method ") + method +
                std::string(" data ") + result).c_str()
            );
        }
        // always cleanup
        curl_easy_cleanup(curl);

        // Check response
        if (response_code < 200 || response_code >= 300) {
            throw std::runtime_error(
                (std::string("curl failed (status code) ") + std::to_string(response_code) +
                std::string(" url ") + url + 
                std::string(" method ") + method +
                std::string(" data ") + result).c_str()
            );
        }

        return result;
    }

    return "";
}
