extern "C" {
    #include "authenticate.h"
    #include "logging.h"

    #include <stdlib.h>
}

#include <iostream>
#include <fstream>
#include <sstream>

#include "request.h"
#include "json.hpp"

using JSON = nlohmann::json;


JSON read_config_file(const std::string & fname) {
    std::ifstream ifs(fname.c_str());
    if (ifs.is_open()) {

        auto config = JSON::parse(ifs);
        
        auto loglevel = config.value("loglevel", "ERROR");
        if (loglevel.compare( "ERROR") == 0) {
            logging_priority = LOG_ERR;
        }
        if (loglevel.compare( "INFO") == 0) {
            logging_priority = LOG_INFO;
        }
        if (loglevel.compare( "DEBUG") == 0) {
            logging_priority = LOG_DEBUG;
        }
        
        return config;
    } else {
        throw std::runtime_error("Could not open file: " + fname);
    }
}

bool check_user(const std::string & local, const std::string & remote) {
    return (strcasecmp(local.c_str(), remote.c_str()) == 0);
}

bool validate_token(const std::string & username, const JSON & well_known, const JSON & config) {
    auto access_token_file = config.value("access_token_file", "/home/" + username + "/.access_token");

    std::ifstream ifs(access_token_file.c_str());
    if (!ifs.is_open()) {
        throw std::runtime_error("Could not open file: " + access_token_file);
    }

    std::stringstream strStream;
    strStream << ifs.rdbuf();
    std::string header("Bearer "+strStream.str());

    if (!header.empty() && header[header.length()-1] == '\n') {
        header.erase(header.length()-1);
    }

    auto userinfo = JSON::parse(
        request(
            well_known.value("userinfo_endpoint", ""),
            "GET", 
            {{"Authorization", header}},
            ""
        )
    );
    
    logging(LOG_DEBUG, "User Info: %s", userinfo.dump().c_str());

    auto user_attribute = config.value("user_attribute", "uid");

    if (! userinfo.contains(user_attribute))
        throw std::runtime_error("Attribute missing: " + user_attribute + ", " + userinfo.dump().c_str());

    auto value = userinfo.at(user_attribute);

    if (value.is_array()) {
        for (int i=0; i<sizeof(value); i++) {
            if (check_user(username, value[i]) == true)
                return true;
        }
        return false;
    } else {
        return check_user(username, value);
    }
}

JSON well_known_introspection(const std::string & wellknown_endpoint) {
    return JSON::parse(request(wellknown_endpoint, "GET", {}, ""));
} 

int authenticate(pam_handle_t *pamh, int flags, int argc, const char **argv) {
    logging_pamh = pamh;
    char *username = NULL;

    if(argc < 1) {
        logging(LOG_ERR, "Missing first argument (configuration file)");
        return PAM_AUTH_ERR;
    }
    
    pam_get_user(pamh, (const char **) &username, "username: ");
    if (!username) {
        return PAM_USER_UNKNOWN;
    }

    try {
        auto config = read_config_file(argv[0]);

        auto well_known = well_known_introspection(config.value("wellknown_endpoint", ""));
        logging(LOG_DEBUG, "Well Known: %s", well_known.dump().c_str());

        if (validate_token(username, well_known, config))
            return PAM_SUCCESS;
    } catch (const std::exception & ex) {
        logging(LOG_ERR, "Exception: %s", ex.what());
    }

    return PAM_AUTH_ERR;
}
